# README #

The full plan as of now is to connect to the paymo api using a provided auth key.
Once authenticated, the admin will have the ability to see all clients and projects.
Any users with authenticated accounts can request access to a client portal, which
will check paymo for the users email. If they match, they can see the projects and
task associated to that user.

Future plans are to allow the users to create tasks and see how much time has been
spent on block maintenance plans, which is a grouping of tasks or lists. 

### What is this repository for? ###

* I'm attempting to learn how to create a Drupal 7 Module by connecting to the Paymo API

### How do I get set up? ###

* Connect as per any other Drupal Module

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Owner is me I guess
* Other community or team contact
